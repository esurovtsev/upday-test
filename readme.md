# Coding task for Upday

Examples of the API:

`GET: http://localhost:8080/api/articles`  
list of all articles

`GET: http://localhost:8080/api/articles?author=evgeny`    
list of all articles for a given author

`GET: http://localhost:8080/api/articles?keyword=java`  
list of all articles for a given keyword

`GET: http://localhost:8080/api/articles?author=evgeny&keyword=java`  
list of all articles for a given author and keyword

`GET: http://localhost:8080/api/articles?publishPeriod=(2014-09-21T20:33:44Z to 2016-01-21T20:33:44Z)`  
list of all articles for a given period


`GET: http://localhost:8080/api/articles/aeda8cd0-2c0e-4897-b4bb-dffa67fdb58b`  
display one article (should be stored before)


`DELETE: http://localhost:8080/api/articles/aeda8cd0-2c0e-4897-b4bb-dffa67fdb58b`  
delete the article


`POST: http://localhost:8080/api/articles`  
create an article  
body:  
```
{
    "header" : "header of the article",
    "description" : "short description",
    "text" : "text of the article, a lot of text here",
    "publishDate": "2014-09-21T20:33:44Z",
    "authors" : ["Evgeny", "Mark"],
    "keywords" : ["java", "spring"]
}
```


`PUT: http://localhost:8080/api/articles/aeda8cd0-2c0e-4897-b4bb-dffa67fdb58b`  
update an article with "full replacement" strategy  
body:  
```
{
    "header" : "header of the article",
    "description" : "short description",
    "text" : "text of the article, a lot of text here",
    "publishDate": "2014-09-21T20:33:44Z",
    "authors" : ["Evgeny", "Mark"],
    "keywords" : ["java", "spring"]
}
```


## Assumptions
Having certain question to the implementation next assumptions about the task were made

* API design was mostly inspired by https://youtu.be/hdSrT4yjS1g
* keywords are plain list of strings
* authors are plain list of strings, where every string is author's name
* there are only 2 representations of the Article: short one and full one. For short representation only "text" field is missing (the rest seems might to be used in the list of articles on frontend)
* search by author and keyword are case sensitive, (example "Evgeny" and "evgeny" are different words)
* update is provided only in the form of "full replacement", meaning update by single fields is not supported

## Restrictions
Having certain time frame for doing the test some restrictions were applied to the final result, namely

* not the whole code was test covered, only critical places (which were not so many) where author expected something to go wrong, were covered by tests
* test was done in the way "simplier is better", so many aspects were ignored, such as reactive services, async io and http, all results return as "plain" fully prepared list of items
* for persistence layer the most simple implementation is selected (the same is true for searching capabilities)
* no data validation is performed (such as empty values for headers and descriptions, other possible logical issues)
* for simplicity no separation of unit and integration tests was made. all tests are marked as unit, even if author is fully aware that the "main" test ArticleControllerTest is integration one
* having time limitations no swagger file or other proper format for API description is provided
* no scripts for filling in initial data set are provided (was not part of the task). Examples API usage can be found in ArticleControllerTest.class

## How to build and run

To build the solution type `mvn clean package`, the resulting artefact can be found in target/updaytest.jar

To run the solution type `java -jar target/updaytest.jar`, a server will start and the solution will be
available on `http://localhost:8080/api/articles`