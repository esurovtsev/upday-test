package com.upday.esurovtsev.model;

import lombok.Builder;
import lombok.Data;
import org.joda.time.DateTime;

import java.util.List;
import java.util.UUID;

@Data
@Builder
public class Article {
    private final UUID id;
    private final String header;
    private final String description;
    private final String text;
    private final DateTime publishDate;
    private final List<String> authors;
    private final List<String> keywords;
}
