package com.upday.esurovtsev.model;

import com.upday.esurovtsev.dtos.DateTimeUtil;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.joda.time.DateTime;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@Builder
public class DateRange {
    private final DateTime from;
    private final DateTime to;

    public static DateRange parse(@NonNull String period) {
        Matcher m = DATE_RANGE_PATTERN.matcher(period);
        if (!m.matches()) {
            throw new IllegalArgumentException(String.format("Given date range is invalid: %s", period));
        }

        DateRange range = DateRange.builder()
                .from(DateTimeUtil.fromUTC(m.group("from")))
                .to(DateTimeUtil.fromUTC(m.group("to")))
                .build();

        if (range.getFrom().isAfter(range.getTo())) {
            throw new IllegalArgumentException(String.format("Given date range is invalid: %s", period));
        }

        return range;
    }

    private final static Pattern DATE_RANGE_PATTERN = Pattern.compile("\\((?<from>.*) (to|TO) (?<to>.*)\\)");
}
