package com.upday.esurovtsev.controllers;

import com.upday.esurovtsev.dtos.ErrorDto;
import com.upday.esurovtsev.exceptions.NotFoundException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
public abstract class ApiController {
    @Autowired
    private HttpServletRequest request;

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDto> handleNotFoundException(Exception e) {
        return new ResponseEntity<>(
                new ErrorDto(HttpStatus.NOT_FOUND.value(), e.getMessage()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorDto> handleIllegalArgumentException(Exception e) {
        return new ResponseEntity<>(
                new ErrorDto(HttpStatus.BAD_REQUEST.value(), e.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorDto> handleRuntimeException(Exception e) {
        return new ResponseEntity<>(
                new ErrorDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    protected String getHrefAttribute(@NonNull String route, @NonNull Object resourceId) {
        return getHrefAttribute(route) + "/" + resourceId;
    }

    protected String getHrefAttribute(@NonNull String route) {
        StringBuffer url = request.getRequestURL();
        String host = url.substring(0, url.indexOf(request.getRequestURI()));
        return host + route;
    }
}
