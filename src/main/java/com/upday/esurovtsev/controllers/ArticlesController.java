package com.upday.esurovtsev.controllers;

import com.google.common.collect.ImmutableList;
import com.upday.esurovtsev.dtos.ArticleDto;
import com.upday.esurovtsev.dtos.ArticleDtoUtil;
import com.upday.esurovtsev.dtos.ArticleListDto;
import com.upday.esurovtsev.exceptions.NotFoundException;
import com.upday.esurovtsev.model.Article;
import com.upday.esurovtsev.model.DateRange;
import com.upday.esurovtsev.services.ArticleService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
@RequestMapping(ArticlesController.ARTICLES_ROUTE)
public class ArticlesController extends ApiController {
    public final static String ARTICLES_ROUTE = "/api/articles";

    @Autowired
    private ArticleService articleService;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody ArticleDto articleDto, HttpServletResponse response) {
        articleDto.setId(null); // forcing to create new entry (=ignore id property from JSON)
        UUID articleId = articleService
                .save(ArticleDtoUtil.toArticle(articleDto))
                .orElseThrow(() -> new RuntimeException("Error when saving article"))
                .getId();
        response.setHeader("Location", articleHrefFn.apply(articleId));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{articleId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("articleId") String articleId) {
        articleService.remove(findArticleByIdOrThrowNotFound(articleId));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{articleId}", method = RequestMethod.PUT)
    public void fullUpdate(@PathVariable("articleId") String articleId, @RequestBody ArticleDto articleDto) {
        Article article = findArticleByIdOrThrowNotFound(articleId);
        articleDto.setId(article.getId()); // force to update exact article (=ignore id property from JSON)
        articleService.save(ArticleDtoUtil.toArticle(articleDto))
                .orElseThrow(() -> new RuntimeException("Error when updating article"));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    public ArticleListDto getAll(
            @RequestParam(value = "author", required = false) String author,
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "publishPeriod", required = false) String publishPeriod) {

        List<Article> articles = articleService.findAll(
                Optional.ofNullable(author),
                Optional.ofNullable(keyword),
                Optional.ofNullable(publishPeriod).map(period -> DateRange.parse(period)));

        ArticleListDto result = new ArticleListDto();
        result.setTotal((long) articles.size());
        result.setItems(toShortArticleDtoList(articles));

        return result;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{articleId}", method = RequestMethod.GET)
    public ArticleDto getById(@PathVariable("articleId") String articleId) {
        return ArticleDtoUtil.fullFromArticle(findArticleByIdOrThrowNotFound(articleId), articleHrefFn);
    }

    private Function<Object, String> articleHrefFn = id -> getHrefAttribute(ArticlesController.ARTICLES_ROUTE, id);

    private List<ArticleDto> toShortArticleDtoList(@NonNull List<Article> items) {
        return items
                .stream()
                .map(article -> ArticleDtoUtil.shortFromArticle(article, articleHrefFn))
                .collect(collectingAndThen(toList(), ImmutableList::copyOf));
    }

    private Article findArticleByIdOrThrowNotFound(@NonNull String articleId) {
        return articleService
                .findById(UUID.fromString(articleId))
                .orElseThrow(() -> new NotFoundException(String.format("Article with id %s was not found", articleId)));
    }
}
