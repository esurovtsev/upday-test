package com.upday.esurovtsev.persistence;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.upday.esurovtsev.model.Article;
import com.upday.esurovtsev.model.DateRange;
import lombok.NonNull;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Component
public class InMemoryArticleDao {
    private Map<UUID, Article> storage = Maps.newHashMap();

    public List<Article> findAll(@NonNull Optional<String> maybeAuthor,
                                 @NonNull Optional<String> maybeKeyword,
                                 @NonNull Optional<DateRange> maybePeriod) {
        return storage
                .values()
                .stream()
                .filter(article -> possibleFilterByList(article.getAuthors(), maybeAuthor))
                .filter(article -> possibleFilterByList(article.getKeywords(), maybeKeyword))
                .filter(article -> possibleFilterByPeriod(article.getPublishDate(), maybePeriod))
                .collect(collectingAndThen(toList(), ImmutableList::copyOf));
    }

    private boolean possibleFilterByPeriod(@NonNull DateTime publishDate, @NonNull Optional<DateRange> maybePeriod) {
        return maybePeriod
                .map(period -> publishDate.isAfter(period.getFrom()) && publishDate.isBefore(period.getTo()))
                .orElse(true);
    }

    private boolean possibleFilterByList(@NonNull List<String> items, @NonNull Optional<String> maybeTerm) {
        return maybeTerm
                .map(term -> items.contains(term))
                .orElse(true);

    }

    public Optional<Article> findById(@NonNull UUID id) {
        return Optional.ofNullable(storage.get(id));
    }

    public void removeById(@NonNull UUID id) {
        storage.remove(id);
    }

    public Optional<Article> save(@NonNull Article article) {
        // might make sense to generalize this functionality and create "copy-builder", but for simplicity leave as it is
        Article toSave = Article.builder()
                .id(article.getId() != null ? article.getId() : UUID.randomUUID())
                .header(article.getHeader())
                .description(article.getDescription())
                .text(article.getText())
                .publishDate(article.getPublishDate())
                .authors(ImmutableList.copyOf(article.getAuthors()))
                .keywords(ImmutableList.copyOf(article.getKeywords()))
                .build();
        storage.put(toSave.getId(), toSave);

        return Optional.of(toSave);
    }

    public void removeAll() {
        storage.clear();
    }
}
