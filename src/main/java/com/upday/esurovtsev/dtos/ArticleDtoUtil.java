package com.upday.esurovtsev.dtos;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.upday.esurovtsev.model.Article;
import lombok.NonNull;
import org.joda.time.DateTime;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

public final class ArticleDtoUtil {
    private ArticleDtoUtil() {
    }

    public static ArticleDto fullFromArticle(@NonNull Article input, @NonNull Function<Object, String> hrefFn) {
        ArticleDto result = new ArticleDto();
        result.setHref(hrefFn.apply(input.getId()));
        result.setId(input.getId());
        result.setHeader(input.getHeader());
        result.setDescription(input.getDescription());
        result.setText(input.getText());
        result.setPublishDate(DateTimeUtil.toUTC(input.getPublishDate()));
        result.setKeywords(ImmutableList.copyOf(MoreObjects.firstNonNull(input.getKeywords(), ImmutableList.of())));
        result.setAuthors(ImmutableList.copyOf(MoreObjects.firstNonNull(input.getAuthors(), ImmutableList.of())));

        return result;
    }

    public static ArticleDto shortFromArticle(@NonNull Article input, @NonNull Function<Object, String> articleHrefFn) {
        ArticleDto articleDto = fullFromArticle(input, articleHrefFn);
        articleDto.setText(null);
        return articleDto;
    }

    public static Article toArticle(@NonNull ArticleDto input) {
        return Article.builder()
                .id(input.getId())
                .header(input.getHeader())
                .description(input.getDescription())
                .text(input.getText())
                .publishDate(input.getPublishDate() != null ?
                        DateTimeUtil.fromUTC(input.getPublishDate()) : DateTime.now())
                .authors(ImmutableList.copyOf(MoreObjects.firstNonNull(input.getAuthors(), ImmutableList.of())))
                .keywords(ImmutableList.copyOf(MoreObjects.firstNonNull(input.getKeywords(), ImmutableList.of())))
                .build();
    }
}
