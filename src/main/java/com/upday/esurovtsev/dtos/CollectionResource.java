package com.upday.esurovtsev.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CollectionResource<T> extends RestResource {
    private Long total;
    private List<T> items;
}
