package com.upday.esurovtsev.dtos;

import lombok.NonNull;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public final class DateTimeUtil {
    private final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z");

    private DateTimeUtil() {
    }

    public static String toUTC(@NonNull DateTime input) {
        return input.toString(DATE_TIME_FORMATTER);
    }

    public static DateTime fromUTC(@NonNull String input) throws IllegalArgumentException {
        return DateTime.parse(input, DATE_TIME_FORMATTER);
    }
}