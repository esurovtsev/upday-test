package com.upday.esurovtsev.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleDto extends RestResource {
    private UUID id;
    private String header;
    private String description;
    private String text;
    private String publishDate;
    private List<String> authors;
    private List<String> keywords;
}
