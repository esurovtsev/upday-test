package com.upday.esurovtsev.services;

import com.upday.esurovtsev.model.Article;
import com.upday.esurovtsev.model.DateRange;
import com.upday.esurovtsev.persistence.InMemoryArticleDao;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ArticleService {
    @Autowired
    private InMemoryArticleDao articleDao;

    public List<Article> findAll(
            @NonNull Optional<String> maybeAuthor,
            @NonNull Optional<String> maybeKeyword,
            @NonNull Optional<DateRange> maybePeriod) {

        return articleDao.findAll(maybeAuthor, maybeKeyword, maybePeriod);
    }

    public Optional<Article> findById(@NonNull UUID id) {
        return articleDao.findById(id);
    }

    public void remove(@NonNull Article article) {
        articleDao.removeById(article.getId());
    }

    public Optional<Article> save(@NonNull Article article) {
        return articleDao.save(article);
    }
}
