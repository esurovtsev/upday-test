package com.upday.esurovtsev.model;

import com.upday.esurovtsev.dtos.DateTimeUtil;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class DateRangeTest {

    @Test(expected = IllegalArgumentException.class)
    public void testParseWrong() throws Exception {
        DateRange.parse("");
    }

    @Test
    public void testParse() throws Exception {
        DateRange actual = DateRange.parse("(2016-09-19T19:35:43Z to 2016-09-22T19:35:43Z)");

        assertThat(actual.getFrom(), equalTo(DateTimeUtil.fromUTC("2016-09-19T19:35:43Z")));
        assertThat(actual.getTo(), equalTo(DateTimeUtil.fromUTC("2016-09-22T19:35:43Z")));
    }
}