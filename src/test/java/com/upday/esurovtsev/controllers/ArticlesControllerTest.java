package com.upday.esurovtsev.controllers;

import com.google.common.collect.ImmutableList;
import com.upday.esurovtsev.dtos.ArticleDto;
import com.upday.esurovtsev.dtos.ArticleListDto;
import com.upday.esurovtsev.persistence.InMemoryArticleDao;
import lombok.NonNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ArticlesControllerTest implements TestData {
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private InMemoryArticleDao articleDao;

    @Before
    public void setUp() throws Exception {
        articleDao.removeAll();
        getTestDataStream().forEach(article -> articleDao.save(article));
    }

    @Test
    public void testFindAllArticles() throws Exception {
        ResponseEntity<ArticleListDto> response = restTemplate.getForEntity("/api/articles", ArticleListDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getTotal(), is(4L));

        List<UUID> actualIds = toUUIDList(response.getBody());
        assertThat(actualIds, containsInAnyOrder(
                UUID.fromString("a6fd3e4c-fdb4-4c9c-8a52-ad4217f829e1"),
                UUID.fromString("aeda8cd0-2c0e-4897-b4bb-dffa67fdb58b"),
                UUID.fromString("7228ac72-f8be-47b2-a06b-2910ae08b318"),
                UUID.fromString("abbd4c8a-adb9-4d9e-a9ac-c084afb7e8f5")));
    }

    @Test
    public void testFindAllArticlesForGivenAuthor() throws Exception {
        ResponseEntity<ArticleListDto> response =
                restTemplate.getForEntity("/api/articles?author=Thomas Nield", ArticleListDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getTotal(), is(2L));

        List<UUID> actualIds = toUUIDList(response.getBody());
        assertThat(actualIds, containsInAnyOrder(
                UUID.fromString("a6fd3e4c-fdb4-4c9c-8a52-ad4217f829e1"),
                UUID.fromString("aeda8cd0-2c0e-4897-b4bb-dffa67fdb58b")));
    }

    @Test
    public void testFindAllArticlesForGivenAuthorNotFound() throws Exception {
        ResponseEntity<ArticleListDto> response =
                restTemplate.getForEntity("/api/articles?author=Evgeny", ArticleListDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getTotal(), is(0L));
    }

    @Test
    public void testFindAllArticlesForSpecificKeyWord() throws Exception {
        ResponseEntity<ArticleListDto> response =
                restTemplate.getForEntity("/api/articles?keyword=lucene", ArticleListDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getTotal(), is(1L));

        List<UUID> actualIds = toUUIDList(response.getBody());
        assertThat(actualIds, containsInAnyOrder(UUID.fromString("7228ac72-f8be-47b2-a06b-2910ae08b318")));
    }

    @Test
    public void testFindAllArticlesForSpecificKeyWordNotFound() throws Exception {
        ResponseEntity<ArticleListDto> response =
                restTemplate.getForEntity("/api/articles?keyword=eclipse", ArticleListDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getTotal(), is(0L));
    }

    @Test
    public void testFindAllArticlesForAuthorAndSpecificKeyWord() throws Exception {
        ResponseEntity<ArticleListDto> response =
                restTemplate.getForEntity("/api/articles?author=Thomas Nield&keyword=parallelization", ArticleListDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getTotal(), is(1L));

        List<UUID> actualIds = toUUIDList(response.getBody());
        assertThat(actualIds, containsInAnyOrder(UUID.fromString("aeda8cd0-2c0e-4897-b4bb-dffa67fdb58b")));
    }

    @Test
    public void testFindAllArticlesForGivenPeriod() throws Exception {
        ResponseEntity<ArticleListDto> response =
                restTemplate.getForEntity("/api/articles?publishPeriod=(2014-09-21T20:33:44Z to 2016-01-21T20:33:44Z)", ArticleListDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getTotal(), is(2L));

        List<UUID> actualIds = toUUIDList(response.getBody());
        assertThat(actualIds, containsInAnyOrder(
                UUID.fromString("abbd4c8a-adb9-4d9e-a9ac-c084afb7e8f5"),
                UUID.fromString("7228ac72-f8be-47b2-a06b-2910ae08b318")));
    }

    @Test
    public void testFindAllReturnsShortView() throws Exception {
        ResponseEntity<ArticleListDto> response =
                restTemplate.getForEntity("/api/articles", ArticleListDto.class);

        assertThat(response.getBody().getItems().get(0).getText(), is(nullValue()));
    }

    @Test
    public void testDisplayOneArticle() throws Exception {
        ResponseEntity<ArticleDto> response =
                restTemplate.getForEntity("/api/articles/7228ac72-f8be-47b2-a06b-2910ae08b318", ArticleDto.class);

        assertThat(response.getBody().getText(), is(notNullValue()));
        assertThat(response.getBody().getHeader(), is("Salmon Run: Custom Sorting in Solr using External Database Data"));
    }

    @Test
    public void testDisplayOneArticleNotFound() throws Exception {
        ResponseEntity<ArticleDto> response =
                restTemplate.getForEntity("/api/articles/7228ac72-f8be-47b2-a06b-29103408b318", ArticleDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    @Test
    public void testDisplayOneArticleInvalidId() throws Exception {
        ResponseEntity<ArticleDto> response =
                restTemplate.getForEntity("/api/articles/invalid-id-code", ArticleDto.class);

        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void testDeleteArticle() throws Exception {
        ResponseEntity<ArticleDto> responseBeforeDelete =
                restTemplate.getForEntity("/api/articles/7228ac72-f8be-47b2-a06b-2910ae08b318", ArticleDto.class);
        assertThat(responseBeforeDelete.getStatusCode(), is(HttpStatus.OK));

        restTemplate.delete("/api/articles/7228ac72-f8be-47b2-a06b-2910ae08b318");

        ResponseEntity<ArticleDto> responseAfterDelete =
                restTemplate.getForEntity("/api/articles/7228ac72-f8be-47b2-a06b-2910ae08b318", ArticleDto.class);
        assertThat(responseAfterDelete.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    @Test
    public void testCreateArticle() throws Exception {
        ArticleDto dto = new ArticleDto();
        dto.setHeader("new Article");
        dto.setDescription("new article description");
        dto.setText("long text is here");
        dto.setAuthors(ImmutableList.of("Evgeny"));

        String createdResource = restTemplate.postForLocation("/api/articles", dto).toString();

        ResponseEntity<ArticleListDto> response =
                restTemplate.getForEntity("/api/articles?author=Evgeny", ArticleListDto.class);
        assertThat(response.getBody().getTotal(), is(1L));

        assertThat(response.getBody().getItems().get(0).getHref(), is(createdResource));

    }

    @Test
    public void testUpdateArticle() throws Exception {
        ResponseEntity<ArticleListDto> responseBeforeUpdate =
                restTemplate.getForEntity("/api/articles?author=Gilad Bracha", ArticleListDto.class);
        ArticleDto articleDtoBeforeUpdate = responseBeforeUpdate.getBody().getItems().get(0);
        assertThat(articleDtoBeforeUpdate.getHeader(), is("Room 101: Cutting out Static"));


        articleDtoBeforeUpdate.setHeader("updated header for article");
        restTemplate.put("/api/articles/" + articleDtoBeforeUpdate.getId(), articleDtoBeforeUpdate);


        ResponseEntity<ArticleDto> responseAfterUpdate =
                restTemplate.getForEntity("/api/articles/" + articleDtoBeforeUpdate.getId(), ArticleDto.class);
        assertThat(responseAfterUpdate.getBody().getHeader(), is("updated header for article"));
    }

    private List<UUID> toUUIDList(@NonNull ArticleListDto articleList) {
        return articleList
                .getItems()
                .stream()
                .map(articleDto -> articleDto.getId())
                .collect(toList());
    }
}