package com.upday.esurovtsev.controllers;

import com.google.common.collect.ImmutableList;
import com.upday.esurovtsev.model.Article;
import org.joda.time.DateTime;

import java.util.UUID;
import java.util.stream.Stream;

public interface TestData {
    default Stream<Article> getTestDataStream() {
        return Stream.of(
                Article.builder()
                        .id(UUID.fromString("a6fd3e4c-fdb4-4c9c-8a52-ad4217f829e1"))
                        .header("Thomas Nield: RxJava - Maximizing Parallelization")
                        .description("Friday, February 19, 2016 RxJava - Maximizing Parallelization Earlier I posted an entry about achieving parallelization with RxJava . But I have learned a ...")
                        .text("RxJava - Maximizing Parallelization\n" +
                                "Earlier I posted an entry about achieving parallelization with RxJava. But I have learned a few things from experiments and the RxJava devs to maximize parallelization and get better performance. This post is going to focus on computation parallelization and not really IO. Computation is where you really have to constrain the number of threads you use, and roughly you get the best performance accounting for the number of cores your computer has.\n" +
                                "Maximizing Computation with Executors\n" +
                                "In my previous article on parallelization, I had a short conversation with David Karnok, the current maintainer of RxJava, in the comments section. I asked him how many threads the computation() Scheduler creates and utilizes. This was his response:\n" +
                                "If [your computer has] 8 cores, this will only use 5 of them. If you have 4 cores, two groups will share the same core.\n" +
                                "Thinking about this, I began to have many questions. I am not an expert on multithreading, but I read most of Java Concurrency in Practice by Brian Goetz a year or so ago. I recall in Chapter 8, Brian talked about sizing thread pools for computation-intensive tasks. Pragmatically, you can follow this formula to roughly get an optimal number of threads. It's not perfect, but its a simple rule to get well-balanced computation performance.\n" +
                                "Nthreads = NCPU + 1\n" +
                                "So if you have 4 cores, that means you should have 5 threads. If you have 8 cores, you should have 9 threads. That extra thread is to fill up idle CPU cycles when the other threads get short moments of idleness. You can get this formula dynamically using a simple Java expression. \n" +
                                "int threadCt = Runtime.getRuntime().availableProcessors() + 1;\n" +
                                "I may be very naive and might be overlooking something. Or maybe the RxJava developers simply had different goals and wanted to keep the number of threads conservative. But I began to suspect Schedulers.computation() was not maximizing parallelization since it was not explicitly designed for it. \n" +
                                "Observable.range(1,1000)\n" +
                                ".flatMap(i -> Observable.just(i)\n" +
                                "int threadCt = Runtime.getRuntime().availableProcessors() + 1;\n" +
                                "You can also use this in tandem with our ExecutorService strategy. Heck, you can throttle the number of GroupedObservables to be equal to the number of threads. If you have 4 cores, you would then have 5 cleanly divided streams of emissions on 5 threads. ")
                        .publishDate(DateTime.now())
                        .authors(ImmutableList.of("Thomas Nield"))
                        .keywords(ImmutableList.of("rxjava", "reactive", "java"))
                        .build(),

                Article.builder()
                        .id(UUID.fromString("aeda8cd0-2c0e-4897-b4bb-dffa67fdb58b"))
                        .header("Thomas Nield: RxJava- Achieving Parallelization")
                        .description("Thursday, November 5, 2015 RxJava- Achieving Parallelization RxJava is often misunderstood when it comes to the asynchronous/multithreaded aspects of it. ...")
                        .text("RxJava- Achieving Parallelization\n" +
                                "RxJava is often misunderstood when it comes to the asynchronous/multithreaded aspects of it. The coding of multithreaded operations is simple, but understanding the abstraction is another thing.\n" +
                                "\n" +
                                "A common question about RxJava is how to achieve parallelization, or emitting multiple items concurrently from an Observable. Of course, this definition breaks the Observable Contract which states that onNext() must be called sequentially and never concurrently by more than one thread at a time. Now you may be thinking \"how the heck am I supposed to achieve parallelization then, which by definition is multiple items getting processed at a time!?\" Widespread misunderstanding of how parallel actually works in RxJava has even led to the discontinuation of the parallel operator. \n" +
                                "\n" +
                                "You can achieve parallelization in RxJava without breaking the Observable contract, but it requires a little understanding of Schedulers and how operators deal with multiple asynchronous sources.\n" +
                                "\n" +
                                "Say you have this simple Observable that emits a range of Integers 1 to 10 upon subscription.\n" +
                                "Observable<Integer> vals = Observable.range(1,10);\n" +
                                "\n" +
                                "vals.subscribe(val -> System.out.println(val);)\n" +
                                "But let's say we are doing some very intense calculation process to each Integer value.\n" +
                                "Observable<Integer> vals = Observable.range(1,10);\n" +
                                "vals.map(i -> intenseCalculation(i))\n" +
                                ".subscribe(val -> System.out.println(val);)\n" +
                                "And for simplicity's sake let's just make intenseCalculation() sleep for a random interval before returning the integer back, simulating an intensive calculation. We will also make it print the current thread the computation is occurring on.")
                        .publishDate(DateTime.now())
                        .authors(ImmutableList.of("Thomas Nield"))
                        .keywords(ImmutableList.of("rxjava", "reactive", "java", "parallelization"))
                        .build(),

                Article.builder()
                        .id(UUID.fromString("7228ac72-f8be-47b2-a06b-2910ae08b318"))
                        .header("Salmon Run: Custom Sorting in Solr using External Database Data")
                        .description("Custom Sorting in Solr using External Database Data Recently, someone (from another project) asked me if I knew how to sort data in Solr by user-entered ...")
                        .text("Recently, someone (from another project) asked me if I knew how to sort data in Solr by user-entered popularity rating data. The data would be entered as thumbs up/down clicks on the search results page and be captured into a RDBMS. Users would be given the option to sort the data by popularity (as reflected by the percentage of thumbs-up ratings vs thumbs-down ratings).\n" +
                                "\n" +
                                "I knew how to do this with Lucene (using a ScoreDocComparator against an in-memory cache of the database table set to a relatively short expiration time), but I figured that since Solr is used so heavily in situations where user feedback is king, there must be something built into Solr, so I promised to get back to him after I did some research (aka googling, asking on the Solr list, etc).\n" +
                                "\n" +
                                "A quick scan on Google pointed me to this Nabble thread, circa 2007, which seemed reasonable, but still required some customizing Solr (code). So I asked on the Solr list, and as expected, Solr did offer a way to do this (without customizing Solr code), using ExternalFileField field types. The post on tailsweep offers more details about this approach.")
                        .publishDate(DateTime.now().minusYears(1))
                        .authors(ImmutableList.of("Salmon Run"))
                        .keywords(ImmutableList.of("java", "solr", "lucene", "sorting"))
                        .build(),


                Article.builder()
                        .id(UUID.fromString("abbd4c8a-adb9-4d9e-a9ac-c084afb7e8f5"))
                        .header("Room 101: Cutting out Static")
                        .description("A place to be (re)educated in Newspeak Sunday, February 17, 2008 Cutting out Static Most imperative languages have some notion of static variable. This is ...")
                        .text("Most imperative languages have some notion of static variable. This is unfortunate, since static variables have many disadvantages. I have argued against static state for quite a few years (at least since the dawn of the millennium), and in Newspeak, I’m finally able to eradicate it entirely. Why is static state so bad, you ask?\n" +
                                "\n" +
                                "Static variables are bad for security. See the E literature for extensive discussion on this topic. The key idea is that static state represents an ambient capability to do things to your system, that may be taken advantage of by evildoers.\n" +
                                "\n" +
                                "Static variables are bad for distribution. Static state needs to either be replicated and sync’ed across all nodes of a distributed system, or kept on a central node accessible by all others, or some compromise between the former and the latter. This is all difficult/expensive/unreliable.")
                        .publishDate(DateTime.now().minusYears(1))
                        .authors(ImmutableList.of("Gilad Bracha"))
                        .keywords(ImmutableList.of("programming", "static"))
                        .build()
        );
    }
}
